<?php 
	$this->load->view('layouts/head');
	$this->load->view('layouts/navbar');
?>

	<h3>Product Entry Form</h3>
	<?php 
		$attributes = array('id' =>'register_form', 'class'=> 'form-horizontal' );
		echo form_open('register/register', $attributes);
	?>

	<div class="form-group">
		<label for="code">Code:</label>
		<input type="text" name="code" class="form-control" id="code" value="<?php echo ; ?>">
	</div>
	<div class="form-group">
		<label for="dosage">Dosage:</label>
		<input type="text" name="dosage" class="form-control" id="dosage" value="<?php echo $given_email; ?>">
	</div>
	<div class="form-group">
		<label for="Products_name">Products:</label>
		<input type="text" name="Products_name" class="form-control" id="Products_name" value="">
	</div>
	<div class="form-group">
		<label for="unit">Unit:</label>
		<input type="text" name="unit" class="form-control" id="unit" value="">
	</div>
	<div class="form-group">
		<label for="tp_with_vat">TP with vat:</label>
		<input type="text" name="tp_with_vat" class="form-control" id="tp_with_vat" value="">
	</div>
	<div class="form-group">
		<label for="quantity">Qty.:</label>
		<input type="text" name="quantity" class="form-control" id="quantity" value="">
	</div>
	<button type="submit" name="submit" class="btn btn-primary">Add</button>
	<?php echo form_close(); ?>
</body>
</html>