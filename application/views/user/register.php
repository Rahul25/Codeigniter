<?php 
	$this->load->view('layouts/head.php');
	$this->load->view('layouts/navbar.php');
?>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h2>Register Form</h2>
			<?php 
				if( $this->session->flashdata('err_msg') ) {
					echo $this->session->flashdata('err_msg');
				}
				if( $this->session->flashdata('reg_err_msg') ) {
					echo $this->session->flashdata('reg_err_msg');
				}
				if( $this->session->flashdata('reg_succ_msg') ) {
					echo $this->session->flashdata('reg_succ_msg');
				}
				if( $this->session->flashdata('mail_err') ) {
					echo $this->session->flashdata('mail_err');
				}
				if( $this->session->flashdata('mail_success') ) {
					echo $this->session->flashdata('mail_success');
				}
			?>

			<?php 
				$given_name = $this->session->userdata('name');
				$given_email = $this->session->userdata('email');
			?>
			
			<?php 
				$attributes = array('id' =>'register_form', 'class'=> 'form-horizontal' );
				echo form_open('register/register', $attributes);
			?>

			<div class="form-group">
				<label for="username">User Name:</label>
				<input type="text" name="username" class="form-control" id="username" value="<?php echo $given_name; ?>">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" class="form-control" id="email" value="<?php echo $given_email; ?>">
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password" value="">
			</div>
			<div class="form-group">
				<label for="con_password">Confirm Password:</label>
				<input type="password" name="con_password" class="form-control" id="con_password" value="">
			</div>
			<button type="submit" name="submit" class="btn btn-primary">register</button>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>


</body>
</html>