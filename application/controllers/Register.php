<?php 

class Register extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index() {
		$this->load->view('user/register');
	}

	public function register() {
		$this->form_validation->set_rules('username', 'username', 'trim|required|is_unique[user.name]|min_length[4]|max_length[15]',
								array( 'required' => 'please provide a %s',
									   'is_unique' => '%s is already used',
									   'min_length' => '%s is too small',
									   'max_length' => '%s is too large' )
		);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[user.email]',
								array( 'required' => 'please provide a %s',
										'is_unique' => '%s is already used' )
		);
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[10]',
								array( 'required' => 'please provide a %s',
									   'min_length' => '%s is too small',
									   'max_length' => '%s is too large' )
		);
		$this->form_validation->set_rules('con_password', 'Confirm password', 'trim|required|matches[password]',
								array( 'required' => 'please provide a %s',
									   'matches' => '%s is incorrect' )
		);

		if($this->form_validation->run() !== false) {
			$email_token = md5(uniqid(rand()));
			$data = array(
				'name' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'email_token' => $email_token
			);
		
			$this->load->library('authentication');
			$result = $this->authentication->send_mail($data['email'], $email_token);
			if ($result) {
				$this->user_model->insert_user($data);
				$data['mail_success'] = 'successfully registered..please check email to activate your account';
				$this->session->set_flashdata($data);
				redirect('register');	
			}else {
				$data['mail_err'] = $result;
				$this->session->set_flashdata($data);
				redirect('register');
			}
		}else{
			$data = array(
				'err_msg' => validation_errors(),
				'name' => $this->input->post('username'),
				'email' => $this->input->post('email')
			);
			$this->session->set_flashdata($data);
			$this->session->set_userdata($data);
			redirect("register");
		}
	}
}

?>